//
//  Preferences.swift
//  Citizen
//
//  Created by zhussupov on 8/11/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

struct Preferences<T> {
  let key: String
  
  var value: T? {
    get { return UserDefaults.standard.object(forKey: key) as? T }
    set { UserDefaults.standard.set(newValue, forKey: key) }
  }
  
  init(_ k: String = #function) {
    key = k
  }
}
