//
//  NetworkConstants.swift
//  Citizen
//
//  Created by zhussupov on 8/11/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import FirebaseDatabase

struct NetworkConstants{
  
  static let emailPrefix = "@citizen.com"
  fileprivate static let rootRefPath = "citizenTest"
  //let rootRefPath = "citizen"
  
  static var rootRef: DatabaseReference {
    get {
      return Database.database().reference().child(rootRefPath)
    }
  }
  
}
