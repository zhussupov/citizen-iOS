//
//  DesignUtil.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import UIKit

class DesignUtil {
  
  static func configureToAppAppearance() {
    UIApplication.shared.statusBarStyle = .lightContent
    UINavigationBar.appearance().barTintColor = .white
  }
  
  static func citizenThemeGreen() -> UIColor {
    return UIColor(red:0.24, green:0.69, blue:0.42, alpha:1.0)
  }
  
  
}
