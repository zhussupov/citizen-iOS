//
//  StringExtension.swift
//  Citizen
//
//  Created by zhussupov on 8/27/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

extension String {
  subscript (i: Int) -> Character? {
    if i < count {
      return self[index(startIndex, offsetBy: i)]
    } else {
      return nil
    }
  }
}
