//
//  UserModel.swift
//  Citizen
//
//  Created by zhussupov on 8/11/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class UserModel: Mappable {
  var phone: String?
  var password: String?
  var name: String?
  var city: String?
  var rank: String?
  
  init(phone: String?, password: String?, name: String?, city: String?, rank: String?) {
    self.phone = phone
    self.password = password
    self.name = name
    self.city = city
    self.rank = rank
  }
  
  init(dataObject: NSDictionary) {
    self.name = dataObject["name"] as? String
    self.phone = dataObject["phone"] as? String
    self.city = dataObject["city"] as? String
    self.rank = dataObject["rank"] as? String
  }
  
  func toDataObject() -> [String : Any?] {
    let data = [
      "name" : name,
      "city" : city,
      "rank" : rank
    ]
    return data
  }
  
}
