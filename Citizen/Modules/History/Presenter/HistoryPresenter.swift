//
//  HistoryPresenter.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class HistoryPresenter: CommonPresenter {
  
  // MARK: Properties
  
  weak var view: HistoryView?
  var router: HistoryWireframe?
  var interactor: HistoryUseCase?
}

extension HistoryPresenter: HistoryPresentation {
  // TODO: implement presentation methods
  func refresh() {
    
  }
  func didTapLeftButton() {
    router?.popBack()
  }
}

extension HistoryPresenter: HistoryInteractorOutput {
  // TODO: implement interactor output methods
}
