//
//  HistoryContract.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

protocol HistoryView: IndicatableView {
  // TODO: Declare view methods
}

protocol HistoryPresentation: class {
  // TODO: Declare presentation methods
  func refresh()
  func didTapLeftButton()
}

protocol HistoryUseCase: class {
  // TODO: Declare use case methods
}

protocol HistoryInteractorOutput: InteractorOutputProtocol {
  // TODO: Declare interactor output methods
}

protocol HistoryWireframe: class {
  // TODO: Declare wireframe methods
  func popBack()
}
