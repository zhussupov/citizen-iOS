//
//  TemplatePresenter.swift
//  Citizen
//
//  Created by zhussupov on 8/23/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class TemplatePresenter: CommonPresenter {
  
  // MARK: Properties
  
  weak var view: TemplateView?
  var router: TemplateWireframe?
  var interactor: TemplateUseCase?
}

extension TemplatePresenter: TemplatePresentation {
  // TODO: implement presentation methods
  func refresh() {
    
  }
  func didTapLeftButton() {
    router?.popBack()
  }
}

extension TemplatePresenter: TemplateInteractorOutput {
  // TODO: implement interactor output methods
}
