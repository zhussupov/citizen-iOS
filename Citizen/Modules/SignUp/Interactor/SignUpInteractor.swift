//
//  SignUpInteractor.swift
//  Citizen
//
//  Created by zhussupov on 8/15/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class SignUpInteractor {
  
  // MARK: Properties
  
  weak var output: SignUpInteractorOutput?
}

extension SignUpInteractor: SignUpUseCase {
  // TODO: Implement use case methods
  
  func verifyPhoneNumber(_ phone: String) {
    
    UserAPIService.verifyPhoneNumberWith(phone: phone) { (verificationID, error) in
      if self.output?.handleError(error as NSError?) == false {
        self.output?.sentSMS(phone)
      }
    }
  }
  
}
