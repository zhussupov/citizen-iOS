//
//  SignUpRouter.swift
//  Citizen
//
//  Created by zhussupov on 8/15/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import UIKit

class SignUpRouter {
  
  // MARK: Properties
  
  weak var view: UIViewController?
  
  // MARK: Static methods
  
  static func setupModule() -> SignUpViewController {
    let viewController = SignUpViewController()
    let presenter = SignUpPresenter()
    let router = SignUpRouter()
    let interactor = SignUpInteractor()
    
    viewController.presenter =  presenter
    
    presenter.view = viewController
    presenter.indicatableView = viewController
    presenter.router = router
    presenter.interactor = interactor
    
    router.view = viewController
    
    interactor.output = presenter
    
    return viewController
  }
}

extension SignUpRouter: SignUpWireframe {
  // TODO: Implement wireframe methods
  
  func presentSMSLogin(userData: UserModel) {
    let vc = SignUpSMSRouter.setupModule()
    vc.userData = userData
    view?.navigationController?.pushViewController(vc, animated: true)
  }
  
  func popBack() {
    self.view?.navigationController?.popViewController(animated: true)
  }
  
}
