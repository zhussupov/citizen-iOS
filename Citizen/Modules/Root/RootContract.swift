//
//  RootContract.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

import UIKit

protocol RootWireFrame: class {
  func presentMainTabScreen(in window: UIWindow)
}
