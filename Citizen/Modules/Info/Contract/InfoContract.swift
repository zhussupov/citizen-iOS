//
//  InfoContract.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

protocol InfoView: IndicatableView {
  // TODO: Declare view methods
}

protocol InfoPresentation: class {
  // TODO: Declare presentation methods
  func refresh()
  func didTapLeftButton()
}

protocol InfoUseCase: class {
  // TODO: Declare use case methods
}

protocol InfoInteractorOutput: InteractorOutputProtocol {
  // TODO: Declare interactor output methods
}

protocol InfoWireframe: class {
  // TODO: Declare wireframe methods
  func popBack()
}
