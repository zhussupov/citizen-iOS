//
//  InfoPresenter.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class InfoPresenter: CommonPresenter {
  
  // MARK: Properties
  
  weak var view: InfoView?
  var router: InfoWireframe?
  var interactor: InfoUseCase?
}

extension InfoPresenter: InfoPresentation {
  // TODO: implement presentation methods
  func refresh() {
    
  }
  
  func didTapLeftButton() {
    router?.popBack()
  }
}

extension InfoPresenter: InfoInteractorOutput {
  // TODO: implement interactor output methods
}
