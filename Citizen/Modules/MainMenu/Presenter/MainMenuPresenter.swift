//
//  MainMenuPresenter.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import UIKit

class MainMenuPresenter: CommonPresenter {
  
  // MARK: Properties
  
  weak var view: MainMenuView?
  var router: MainMenuWireframe?
  var interactor: MainMenuUseCase?
}

extension MainMenuPresenter: MainMenuPresentation {
  // TODO: implement presentation methods
  func refresh() {
    
  }
  
  func didSelect(presentationData: MenuCellPresentation) {
    switch presentationData.type {
    case .profile:
      router?.showProfile()
    case .info:
      router?.showInfo()
    case .history:
      router?.showHistory()
    case .exit:
      view?.showActivityIndicator()
      interactor?.signOut()
    }
  }
}

extension MainMenuPresenter: MainMenuInteractorOutput {
  // TODO: implement interactor output methods
  func signOutSuccess() {
    view?.hideActivityIndicator()
    UserProfileManager.shared.userLoggedOut()
  }
}
