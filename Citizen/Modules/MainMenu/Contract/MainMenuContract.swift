//
//  MainMenuContract.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

protocol MainMenuView: IndicatableView {
  // TODO: Declare view methods
}

protocol MainMenuPresentation: class {
  // TODO: Declare presentation methods
  func refresh()
  func didSelect(presentationData: MenuCellPresentation)
}

protocol MainMenuUseCase: class {
  // TODO: Declare use case methods
  func signOut()
}

protocol MainMenuInteractorOutput: InteractorOutputProtocol {
  // TODO: Declare interactor output methods
  func signOutSuccess()
}

protocol MainMenuWireframe: class {
  // TODO: Declare wireframe methods
  func showProfile()
  func showInfo()
  func showHistory()
  func exit()
}
