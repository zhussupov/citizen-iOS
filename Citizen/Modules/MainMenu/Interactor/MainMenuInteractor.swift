//
//  MainMenuInteractor.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class MainMenuInteractor {
  
  // MARK: Properties
  
  weak var output: MainMenuInteractorOutput?
}

extension MainMenuInteractor: MainMenuUseCase {
  // TODO: Implement use case methods
  func signOut() {
    UserAPIService.signOut { (error) in
      if self.output?.handleError(error as NSError?) == false {
        self.output?.signOutSuccess()
      }
    }
  }
}
