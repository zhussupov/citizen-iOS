//
//  ProfileInteractor.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class ProfileInteractor {
  
  // MARK: Properties
  
  weak var output: ProfileInteractorOutput?
}

extension ProfileInteractor: ProfileUseCase {
  // TODO: Implement use case methods
  func fetchProfileInfo() {
    UserAPIService.fetchProfileInfo { (response, error) in
      if self.output?.handleError(error as NSError?) == false {
        if let data = response {
          let userData = UserModel(dataObject: data)
          self.output?.gotProfileInfo(data: userData)
        }
      }
    }
  }
}
