//
//  ProfilePresenter.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class ProfilePresenter: CommonPresenter {
  
  // MARK: Properties
  
  weak var view: ProfileView?
  var router: ProfileWireframe?
  var interactor: ProfileUseCase?
}

extension ProfilePresenter: ProfilePresentation {
  // TODO: implement presentation methods
  
  func viewDidLoad() {
    view?.showActivityIndicator()
    interactor?.fetchProfileInfo()
  }
  
  func refresh() {
    
  }
  
  func didTapLeftButton() {
    router?.popBack()
  }
}

extension ProfilePresenter: ProfileInteractorOutput {
  // TODO: implement interactor output methods
  func gotProfileInfo(data: UserModel) {
    view?.hideActivityIndicator()
    view?.updateWith(userData: data)
  }
}
