//
//  SignInRouter.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import UIKit
import SideMenuSwift

class SignInRouter {
  
  // MARK: Properties
  
  weak var view: UIViewController?
  
  // MARK: Static methods
  
  static func setupModule() -> SignInViewController {
    let viewController = SignInViewController()
    let presenter = SignInPresenter()
    let router = SignInRouter()
    let interactor = SignInInteractor()
    
    viewController.presenter =  presenter
    
    presenter.view = viewController
    presenter.indicatableView = viewController
    presenter.router = router
    presenter.interactor = interactor
    
    router.view = viewController
    
    interactor.output = presenter
    
    return viewController
  }
}

extension SignInRouter: SignInWireframe {
  // TODO: Implement wireframe methods
  
  func presentMainMenu() {
    RootRouter().presentMainTabScreen(in: UIApplication.shared.keyWindow!)
  }
  
  func presentSignUpPage() {
    let vc = SignUpRouter.setupModule()
    view?.navigationController?.pushViewController(vc, animated: true)
  }
  
  func presentRestorePassword() {
    let vc = ForgotPasswordRouter.setupModule()
    view?.navigationController?.pushViewController(vc, animated: true)
  }
}
