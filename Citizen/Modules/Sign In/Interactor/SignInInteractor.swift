//
//  SignInInteractor.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import FirebaseAuth

class SignInInteractor {
  
  // MARK: Properties
  
  weak var output: SignInInteractorOutput?
}

extension SignInInteractor: SignInUseCase {
  // TODO: Implement use case methods
  
  func signIn(phone: String, password: String) {
    UserAPIService.signInWith(phoneNumber: phone, password: password) { (authResult, error) in
      if self.output?.handleError(error as NSError?) == false {
        self.output?.signInSuccess()
      }
    }
  }
  
}
