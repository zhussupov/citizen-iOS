//
//  SignUpSMSInteractor.swift
//  Citizen
//
//  Created by zhussupov on 8/23/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import FirebaseAuth

class SignUpSMSInteractor {
  
  // MARK: Properties
  
  weak var output: SignUpSMSInteractorOutput?
}

extension SignUpSMSInteractor: SignUpSMSUseCase {
  // TODO: Implement use case methods
  
  func signUp(verificationCode: String, userData: UserModel) {
    
    if let verificationID = UserProfileManager.shared.verifiactionId.value {
      
      UserAPIService.signUpWith(verificationID: verificationID, verificationCode: verificationCode, userData: userData) { (result, error) in
        
        if self.output?.handleFirebaseError(error as NSError?, completion: {
          self.output?.goBack()
        }) == false {
          self.output?.signUpSuccess()
        }
      }
    }
  }
}
