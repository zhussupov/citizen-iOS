//
//  ChangePasswordContract.swift
//  Citizen
//
//  Created by zhussupov on 8/28/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

protocol ChangePasswordView: IndicatableView {
  // TODO: Declare view methods
}

protocol ChangePasswordPresentation: class {
  // TODO: Declare presentation methods
  func refresh()
  func didTapClose()
  func didTapChange(password: String)
}

protocol ChangePasswordUseCase: class {
  // TODO: Declare use case methods
  func changePasswordTo(password: String, phone: String)
}

protocol ChangePasswordInteractorOutput: InteractorOutputProtocol {
  // TODO: Declare interactor output methods
  func changeSuccess()
}

protocol ChangePasswordWireframe: class {
  // TODO: Declare wireframe methods
  func popBack()
}
