//
//  ChangePasswordRouter.swift
//  Citizen
//
//  Created by zhussupov on 8/28/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordRouter {
  
  // MARK: Properties
  
  weak var view: UIViewController?
  
  // MARK: Static methods
  
  static func setupModule(phone: String?, completion: (() -> Void)?, onBackAction: (() -> Void)?) -> ChangePasswordViewController {
    let viewController = ChangePasswordViewController()
    let presenter = ChangePasswordPresenter()
    let router = ChangePasswordRouter()
    let interactor = ChangePasswordInteractor()
    
    viewController.presenter =  presenter
    
    presenter.successCompletion = completion
    presenter.onBackAction = onBackAction
    presenter.phone = phone
    
    presenter.view = viewController
    presenter.indicatableView = viewController
    presenter.router = router
    presenter.interactor = interactor
    
    router.view = viewController
    
    interactor.output = presenter
    
    return viewController
  }
}

extension ChangePasswordRouter: ChangePasswordWireframe {
  // TODO: Implement wireframe methods
  
  func popBack() {
    self.view?.navigationController?.popViewController(animated: true)
  }
  
}
