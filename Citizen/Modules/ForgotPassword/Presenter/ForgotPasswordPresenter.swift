//
//  ForgotPasswordPresenter.swift
//  Citizen
//
//  Created by zhussupov on 8/28/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

class ForgotPasswordPresenter: CommonPresenter {
  
  // MARK: Properties
  
  weak var view: ForgotPasswordView?
  var router: ForgotPasswordWireframe?
  var interactor: ForgotPasswordUseCase?
}

extension ForgotPasswordPresenter: ForgotPasswordPresentation {
  // TODO: implement presentation methods
  func refresh() {
    
  }
  
  func didTapClose() {
    router?.popBack()
  }
  
  func didTapForget(phone: String) {
    router?.presentPhoneVerification(phone: phone)
  }
  
  func checkIfUserExists(_ phone: String) {
    UserAPIService.checkIfUserExists(phoneNumber: phone) { (exists, error) in
      if self.handleError(error as NSError?) == false {
        if let exists = exists {
          self.view?.updateWith(exists: exists)
        }
      }
    }
  }
}

extension ForgotPasswordPresenter: ForgotPasswordInteractorOutput {
  // TODO: implement interactor output methods
  
}
