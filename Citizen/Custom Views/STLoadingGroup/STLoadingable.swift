//
//  STLoadingable.swift
//  Citizen
//
//  Created by zhussupov on 8/17/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import UIKit

typealias STEmptyCallback = () -> ()

protocol STLoadingable {
  var isLoading: Bool { get }
  
  func startLoading()
  func stopLoading(finish: STEmptyCallback?)
}

protocol STLoadingConfig {
  var animationDuration: TimeInterval { get }
  var lineWidth: CGFloat { get }
  var loadingTintColor: UIColor { get }
}
