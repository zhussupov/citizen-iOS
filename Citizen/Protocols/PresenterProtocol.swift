//
//  PresenterProtocol.swift
//  Citizen
//
//  Created by zhussupov on 8/14/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

protocol InteractorOutputProtocol: class {
  @discardableResult
  func handleError(_ error: NSError?) -> Bool
  
  @discardableResult
  func handleErrorMessage(_ error: NSError?) -> Bool
  
  @discardableResult
  func handleFirebaseError(_ error: NSError?, completion: ( () -> Void)?) -> Bool
}
