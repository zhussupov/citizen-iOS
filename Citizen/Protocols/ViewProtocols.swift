//
//  ViewProtocols.swift
//  Citizen
//
//  Created by zhussupov on 8/11/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

protocol IndicatableView: class {
  func showActivityIndicator()
  func showError(with message: String)
  func show(_ message: String)
  func showNetworkError()
  func hideActivityIndicator()
  func hideActivityIndicatorWith(completion: @escaping() -> Void)
  func showSuccess()
  func showAlertWith(message: String, completion: (() -> Void)?)
}

protocol NetworkHandlingView: class {
  func showNoNetwork()
  func hideNoNetwork()
  func showEmptyScreen()
}
