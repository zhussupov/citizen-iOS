//
//  MappableProtocol.swift
//  Citizen
//
//  Created by zhussupov on 8/24/18.
//  Copyright © 2018 Zhussupov. All rights reserved.
//

import Foundation

protocol Mappable: class {
  func toDataObject() -> [String : Any?]
}
